// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import router from './router'
import '../static/layer/theme/default/layer.css'
import 'iview/dist/styles/iview.css'
import './assets/common.css'
import {Spin, Row, Col, Table, Tabs, TabPane, Card, Form, FormItem, Input, DatePicker, Button, Message, Select, Option, Page, Icon,Checkbox,CheckboxGroup,DropdownMenu,DropdownItem,Dropdown } from 'iview'

Vue.component('Table', Table)
Vue.component('Row', Row)
Vue.component('Col', Col)
Vue.component('Tabs', Tabs)
Vue.component('TabPane', TabPane)
Vue.component('Card', Card)
Vue.component('Form', Form)
Vue.component('FormItem', FormItem)
Vue.component('Input', Input)
Vue.component('DatePicker', DatePicker)
Vue.component('Button', Button)
Vue.component('Select', Select)
Vue.component('Option', Option)
Vue.component('Page', Page)
Vue.component('Icon', Icon)
Vue.component('CheckboxGroup', CheckboxGroup)
Vue.component('Checkbox', Checkbox)
Vue.component('CheckboxGroup', CheckboxGroup)
Vue.component('DropdownMenu', DropdownMenu)
Vue.component('DropdownItem', DropdownItem)
Vue.component('Dropdown', Dropdown)
Vue.component('Spin', Spin)

// Vue.component('Ibutton', Button)

// Vue.component('Message', Message)

Vue.prototype.$Message = Message

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data: {
    Bus: new Vue()
  },
  components: { App },
  template: '<App/>'
})
