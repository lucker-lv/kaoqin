import ReconnectingWebSocket from 'reconnectingwebsocket'
/* eslint no-extend-native: ["error", { "exceptions": ["Date"] }] */
export default {
    // // 时间戳转日期时间,返回 2018-03-29
    tampToTime(stamp) {
        var T = new Date(stamp * 1000),
            Format = function (Q) { return Q < 10 ? '0' + Q : Q },
            Result = Format(T.getFullYear()) + "-" + Format(T.getMonth() + 1) + "-" + Format(T.getDate());
        return Result;
    },
    //转时间戳: 可以传"2018-05-06" 或 "2018-05-06 10:02:01"
    //单传日期: 返回该天的 时间为 0:0:0 的时间戳
    dateTransverter: function (s) {
        if (typeof s == "string") {
            var date = new Date(s.replace(/-/g, '/'));
            return Date.parse(date) / 1000;
        }
        throw new Error("你传入的时间不是一个字符串,无法转换!");
    },
    checkinColumns: [
        {
            title: '时间',
            key: 'time'
        },
        {
            title: '姓名',
            key: 'username'
        },
        {
            title: '状态',
            key: 'status'
        }
    ],
    listColumns: [
        {
            title: '时间',
            key: 'wtime',
            sortable: true,
            minWidth: 180
        },
        {
            title: '姓名',
            key: 'username',
            minWidth: 90
        },
        {
            title: '员工编号',
            key: 'userid',
            minWidth: 120,
            sortable: true,
            sortMethod: (a, b, type) => {
                return type === 'asc' ? a - b : b - a
            }
        },
        {
            title: '部门',
            key: 'department',
            minWidth: 180
        },
        {
            title: '职位',
            key: 'position',
            minWidth: 82
        },
        {
            title: '状态',
            key: 'wstatus',
            minWidth: 100,
            fixed: 'right'
        }
    ],
    // webSocket请求封装
    webSocketEvent(url, param, fn) {
        var _this = this
        if ('WebSocket' in window) {
            var ws = new ReconnectingWebSocket(url)
            ws.onopen = function () {
                ws.send(JSON.stringify(param))
            }
            ws.onmessage = function (res) {
                fn(res.data);
            }
            ws.onclose = function () {
                _this.isOnline = false
            }
            // 监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
            window.onbeforeunload = function () {
                ws.close();
            }
            return ws
        } else {
            alert(
                '您的浏览器版本过低,请升级到最新版本!推荐使用谷歌或者火狐浏览器.'
            )
        }
    },

    // 删除指定的 key == value 的对象
    // 调用 objectFilter(this.staffData,"status","上班打卡");
    objectFilter(arr, key, value) {
        // callee 在严格模式用不了, 用闭包代替
        function foo() {
            for (var i = 0, len = arr.length; i < len; i++) {
                if (arr[i][key] == value) {
                    arr.splice(i, 1);
                    foo(arr);
                    break;
                }
            }
        }

        foo();
    },
    // 判断已有数组里面的对象: 如果key值 == obj的key值, 就移除该对象
    // use:  1 isRepeat(arr, obj, "urerid");
    //       2 isRepeat(arr, obj, ["userid", "status"]); // 两个key 值同时相等
    isRepeat(arr, obj, key) {
        var key = typeof key == "string" ? new Array(key) : key;
        !function foo() {
            var len = arr.length;
            for (var j = 0; j < len; j++) {
                var flag = false;
                for (var i = 0; i < key.length; i++) {
                    if (arr[j][key[i]] == obj[key[i]]) {
                        flag = true;
                    } else {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    arr.splice(j, 1);
                    foo();
                    break;
                }
            }
        }();
    },
}