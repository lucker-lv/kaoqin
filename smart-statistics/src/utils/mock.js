export default {
    title: [
        {
            title: '时间',
            key: 'time'
        },
        {
            title:'姓名',
            key:'name'
        },
        {
            title:'员工编号',
            key:'num',
        },
        {
            title:'部门',
            key:'department'
        },
        {
            title:'职位',
            key:'job'
        },
        {
            title:'状态',
            key:'status',
        },
    ],
    data: [
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工018',
            department:'销售部门',
            job: '采花贼',
            status: '不在',
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工024',
            department:'销售部门',
            job: '航母维修',
            status: '早退'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工026',
            department:'销售部门',
            job: '核弹头翻新',
            status: '迟到'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工018',
            department:'销售部门',
            job: '采花贼',
            status: '不在'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工024',
            department:'销售部门',
            job: '航母维修',
            status: '早退'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工026',
            department:'销售部门',
            job: '核弹头翻新',
            status: '迟到'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工018',
            department:'销售部门',
            job: '采花贼',
            status: '不在'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工024',
            department:'销售部门',
            job: '航母维修',
            status: '早退'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工026',
            department:'销售部门',
            job: '核弹头翻新',
            status: '迟到'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工018',
            department:'销售部门',
            job: '采花贼',
            status: '不在'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工024',
            department:'销售部门',
            job: '航母维修',
            status: '早退'
        },
        {
            time: '2018-04-17 14:05:32',
            name: 'Jim Green',
            num: '员工026',
            department:'销售部门',
            job: '核弹头翻新',
            status: '迟到'
        },
    ],
    personTitle:[
        {
            title: '时间',
            key: 'time'
        },
        {
            title: '姓名',
            key: 'username'
        },
        {
            title: '状态',
            key: 'status'
        }
    ],
    personData: [
        {
            time: '2018-04-17 14:05:32',
            username: 'Jum',
            num: '员工018',
            department:'销售部门',
            job: '采花贼',
            status: '上班打卡',
        },
        {
            time: '2018-04-17 14:05:32',
            username: 'Ject',
            num: '员工024',
            department:'销售部门',
            job: '航母维修',
            status: '上班打卡'
        },
        {
            time: '2018-04-17 14:05:32',
            username: 'Leave',
            num: '员工024',
            department:'销售部门',
            job: '航母维修',
            status: '上班打卡'
        },
        {
            time: '2018-04-17 14:05:32',
            username: 'aaron',
            num: '员工026',
            department:'销售部门',
            job: '核弹头翻新',
            status: '上班打卡'
        }

    ],
}