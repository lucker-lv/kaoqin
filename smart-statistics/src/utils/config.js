export default {
  wsUrl: 'ws://103.251.36.122:9801',
  // wsUrl: 'ws://123.207.167.163:9010/ajaxchattest', //测试接口
  param: {
    type: 'getcheckon'
  },
  historyParam: {
    type: 'getcheckonhistory'
  },
  newinParam: {
    type: 'getnewin'
  },
  searchUrl: 'http://103.251.36.122:9806?do=getdaywork',
  nowStatusUrl: 'http://103.251.36.122:9806/?do=getnowstatus',
  // 统计图表,参数: day=2018-04-17
  day: 'http://103.251.36.122:9806/?do=getworktall'
}
