import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'PageHome',
      component: () => import('@/components/PageHome')
    },
    {
      path: '/guest',
      name: 'PageGuest',
      component: () => import('@/components/PageGuest')
    }
  ]
})

