var aCm = [];//定制菜单配置数组

//数据显示样式
var dataStyle = {
	normal: {
		label: {
			show: false
		},
		labelLine: {
			show: false
		},
		shadowBlur: 40,
		shadowColor: 'rgba(40, 40, 40, 0.5)',
	}
};

//图表显示样式
var placeHolderStyle = {
	normal: {
		color: 'rgba(44,59,70,1)', // 未完成的圆环的颜色
		label: {
			show: false
		},
		labelLine: {
			show: false
		}
	},
	emphasis: {
		color: 'rgba(44,59,70,1)' // 未完成的圆环的颜色
	}
};

//图表配置
var option = {
	title: {
		text: '考勤状况',
		left: "center",
		textStyle: {
			fontWeight: 'normal',
			color: '#fff',
			fontSize: 18
		}
	},
	tooltip: {
		show: true,
	},
	toolbox: {
		show: true,
	},
	
	backgroundColor: '#000',
	series: [
		{
			
			type: 'pie',
			clockWise: true,
			radius: [80, 85],
			itemStyle: dataStyle,
			hoverAnimation: true,
			stillShowZeroSum:true,
			center: ['50%', '12%'],
			data: [
					{
						
						value: 25,
						name:"出勤人数",
						label: {
							normal: {
								formatter: function(p) {
									return "出勤率：25%";
								},
								position: 'center',
								show: true,
								textStyle: {
									fontSize: '15',
									fontWeight: 'normal',
									color: '#3dd4de'
								}
							}
						},
						tooltip: {
							show: true,
							formatter: function(p) {
								return "出勤人数：" + p.value;
							},
						},
						itemStyle: {
							normal: {
								color: '#3dd4de',
								shadowColor: '#3dd4de',
								shadowBlur: 10
							}
						}
					}, 
					{
						value: 75,
						name: '无出勤人数',
						itemStyle: placeHolderStyle,
						tooltip: {
							show: true,
							formatter: function(p) {
								return "无出勤人数：" + p.value;
							},
						},
					}
				]
		},
		{
			type: 'pie',
			clockWise: true,
			radius: [80, 85],
			itemStyle: dataStyle,
			hoverAnimation: true,
			center: ['50%', '30%'],
			data: [
					{						
						value: 50,
						name: '迟到人数',
						itemStyle: {
							normal: {
								color: '#b697cd',
								shadowColor: '#b697cd',
								shadowBlur: 10
							}
						},
						tooltip: {
							show: true,
							formatter: function(p) {
								return "迟到人数：" + p.value;
							},
						},
						label: {
							normal: {
								formatter: function(p) {
									return "迟到率：50%";
								},
								position: 'center',
								show: true,
								textStyle: {
									fontSize: '15',
									fontWeight: 'normal',
									color: '#b697cd'
								}
							}
						},
						
					}, 
					{						
						value: 50,
						name: '无迟到人数',
						itemStyle: placeHolderStyle,
						tooltip: {
							show: true,
							formatter: function(p) {
								return "无迟到人数：" + p.value;
							},
						},
					}
				]
		},
		{
			type: 'pie',
			clockWise: true,
			radius: [80, 85],
			itemStyle: dataStyle,
			hoverAnimation: true,
			center: ['50%', '48%'],
			data: [
					{						
						value: 40,
						name:"早退人数",
						label: {
							normal: {
								formatter: function(p) {
									return "早退率：40%";
								},
								position: 'center',
								show: true,
								textStyle: {
									fontSize: '15',
									fontWeight: 'normal',
									color: '#a6f08f'
								}
							}
						},
						tooltip: {
							show: true,
							formatter: function(p) {
								return "早退人数：" + p.value;
							},
						},
						itemStyle: {
							normal: {
								color: '#a6f08f',
								shadowColor: '#a6f08f',
								shadowBlur: 10
							}
						}
					}, {
						value: 60,
						name: '无早退人数',
						itemStyle: placeHolderStyle,
						tooltip: {
							show: false,
						},
						tooltip: {
							show: true,
							formatter: function(p) {
								return "无早退人数：" + p.value;
							},
						},
					}
				]
		},
		{
			type: 'pie',
			clockWise: true,
			radius: [80, 85],
			itemStyle: dataStyle,
			hoverAnimation: true,
			center: ['50%', '66%'],
			data: [
					{						
						value: 40,
						name:"缺卡人数",
						label: {
							normal: {
								formatter: function(p) {
									return "缺卡率：40%";
								},
								position: 'center',
								show: true,
								textStyle: {
									fontSize: '15',
									fontWeight: 'normal',
									color: '#a6f11f'
								}
							}
						},
						tooltip: {
							show: true,
							formatter: function(p) {
								return "缺卡人数：" + p.value;
							},
						},
						itemStyle: {
							normal: {
								color: '#a53333',
								shadowColor: '#a53333',
								shadowBlur: 10
							}
						}
					}, {
						value: 60,
						name: '无缺卡人数',
						itemStyle: placeHolderStyle,
						tooltip: {
							show: false,
						},
						tooltip: {
							show: true,
							formatter: function(p) {
								return "无缺卡人数：" + p.value;
							},
						},
					}
				]
		},
		{
			type: 'pie',
			clockWise: true,
			radius: [80, 85],
			itemStyle: dataStyle,
			hoverAnimation: true,
			center: ['50%', '84%'],
			data: [
					{						
						value: 40,
						name:"旷工人数",
						label: {
							normal: {
								formatter: function(p) {
									return "旷工率：40%";
								},
								position: 'center',
								show: true,
								textStyle: {
									fontSize: '15',
									fontWeight: 'normal',
									color: '#b1a11f'
								}
							}
						},
						tooltip: {
							show: true,
							formatter: function(p) {
								return "旷工人数：" + p.value;
							},
						},
						itemStyle: {
							normal: {
								color: '#2196f3',
								shadowColor: '#2196f3',
								shadowBlur: 10
							}
						}
					}, {
						value: 60,
						name: '无旷工人数',
						itemStyle: placeHolderStyle,
						tooltip: {
							show: false,
						},
						tooltip: {
							show: true,
							formatter: function(p) {
								return "无旷工人数：" + p.value;
							},
						},
					}
				]
		},
	]
};

//初始化图表
var myChart2 = echarts.init(document.getElementById('main2'));
myChart2.setOption(option);

//打开定制菜单
mui('.mui-scroll-wrapper').scroll();
mui("#filtrateList").on('change','.mui-table-view-cell input',function(){
	aCm = [];
	$("#filtrateList input").each(function(i){
		console.log($(this).prop("checked"),$(this).attr("data-num"));
		aCm.push({"isShow":$(this).prop("checked"),"sort":$(this).attr("data-num")});
	});
	myChart2.resize();
});