"use strict"

//server ip
var sIp = '103.251.36.122';

/*
 底部选项卡切换
 * */
mui('nav').on('tap', '#onlineTime', function() {
	mui.openWindow({
		id: 'onlineTime',
		url: 'onlineTime.html'
	});
});

mui("nav").on('tap', '#search', function() {
	//获取id
	//		  var id = this.getAttribute("id");
	//		  //传值给详情页面，通知加载新数据
	//		  mui.fire(detail,'getDetail',{id:id});
	//打开新闻详情
	mui.openWindow({
		id: 'search',
		url: 'search.html'
	});
});

mui('nav').on('tap', '#abnormal', function() {
	mui.openWindow({
		id: 'abnormal',
		url: 'abnormal.html'
	});
});

mui('nav').on('tap', '#statistics', function() {
	mui.openWindow({
		id: 'statistics',
		url: 'statistics.html'
	});
});



/*
 获取当前静态所有时间
 参数：
 'y-m-d' ==> 年月日
 'm-d' ==> 年月
 'm-d' ==> 月日
 'h-m-s' ==> 时分秒
 'h-m' ==> 时分
 'm-s' ==> 分秒
 'w' ==>星期
 '' ==>年月日 时分秒
 * */
function getOnTime(oTime) {
	//获取当前具体时间
	var oDate = new Date();
	var nYear = oDate.getFullYear();
	var nMonth = oDate.getMonth() * 1 + 1;
	var nDate = oDate.getDate();

	var nHours = oDate.getHours();
	var nMinutes = oDate.getMinutes();
	var nSeconds = oDate.getSeconds();

	(nHours < 10) && (nHours = "0" + nHours);
	(nMinutes < 10) && (nMinutes = "0" + nMinutes);
	(nSeconds < 10) && (nSeconds = "0" + nSeconds);

	switch(true) {
		case(oTime === 'y-m-d'):
			return nYear + "-" + nMonth + "-" + nDate;
			break;
		case(oTime === 'y-m'):
			return nYear + "-" + nMonth;
			break;
		case(oTime === 'm-d'):
			return nMonth + "-" + nDate;
			break;
		case(oTime === 'h-m-s'):
			return nHours + ":" + nMinutes + ":" + nSeconds;
			break;
		case(oTime === 'm-s'):
			return nMinutes + ":" + nSeconds;
			break;
		case(oTime === 'h-m'):
			return nHours + ":" + nMinutes;
			break;
		case(oTime === 'w'):
			return "今天是星期" + "日一二三四五六".charAt(new Date().getDay());
			break;
		default:
			return nYear + "-" + nMonth + "-" + nDate + "\0" + nHours + ":" + nMinutes + ":" + nSeconds;
	}
}


//转换时间搓
function dateTransverter(s){
	if(s){
		var date = new Date(s.replace(/-/g, '/'));
		return Date.parse(date)/1000;
	}
}