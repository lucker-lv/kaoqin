	//年月日日期表单选择器
	mui(".mui-date").each(function(i, btn) {
		btn.addEventListener('tap', function() {
			var _this = this;
			var optionsJson = this.getAttribute('data-options') || '{}';
			var options = JSON.parse(optionsJson);
			var picker = new mui.DtPicker(options);
			picker.show(function(rs) {
				/*
				 * rs.value 拼合后的 value
				 * rs.text 拼合后的 text
				 * rs.y 年，可以通过 rs.y.vaue 和 rs.y.text 获取值和文本
				 * rs.m 月，用法同年
				 * rs.d 日，用法同年
				 * rs.h 时，用法同年
				 * rs.i 分（minutes 的第二个字母），用法同年
				 */

				_this.value = rs.text;
				/* 
				 * 返回 false 可以阻止选择框的关闭
				 * return false;
				 */
				/*
				 * 释放组件资源，释放后将将不能再操作组件
				 * 通常情况下，不需要示放组件，new DtPicker(options) 后，可以一直使用。
				 * 当前示例，因为内容较多，如不进行资原释放，在某些设备上会较慢。
				 * 所以每次用完便立即调用 dispose 进行释放，下次用时再创建新实例。
				 */
				picker.dispose();
			});
		});
	});
	
	var oSureBtn = document.getElementById("sureBtn");
	var oStartDate = document.getElementById("startDate");
	var oEndDate = document.getElementById("endDate");
	var oStatus = document.getElementById("status");
	var oKeyword = document.getElementById("keyword");
	var oSearchIcon = document.getElementById("searchIcon");
	
	//点击请求搜索的时候
	oSureBtn.addEventListener('tap', function() {
		if(startDate.value == "" || endDate.value == ""){
			mui.alert('两个时间点都必须选上');
		}else{

			$(function(){
				$.ajax({
					type:"get",
					url:'http://'+ sIp +':9806',
					async:true,
					data:backRequert(),
					timeout:10000,//超时时间设置为10秒；      
					success:function(data){
						if(JSON.parse(data).ResultCode == "SUCCESS"){
							document.getElementById("formBox").style.display = "none";
							createList(JSON.parse(data).Result,$("#searchList"));
						}
						
					},
					error:function(data){
						mui.alert('服务器故障，请稍后再试')
					}
				});
			});

		}
	});
	
	
	//显示搜索菜单
	oSearchIcon.addEventListener('tap', function() {
		document.getElementById("formBox").style.display = "block";
		document.getElementById("searchList").innerHTML = "";
	});
	
	//请求参数配置
	function backRequert(){
		if(oKeyword.value == ""){
			return {
				"do":"getdaywork",
				"startday":oStartDate.value,
				"endday":oEndDate.value,
				"status":oStatus.value
			};
		}else{
			return {
				"do":"getdaywork",
				"startday":oStartDate.value,
				"endday":oEndDate.value,
				"status":oStatus.value,
				"keyword":oKeyword.value
			};
		}
	}

	//渲染列表
	function createList(aData,container){
		container.html("");
		$.each(aData, function(i,v) {
			var sLi = '<li class="mui-table-view-cell mui-media">'+
					'<a href="javascript:;" class="mui-navigate-right">' +
					'<div class="mui-media-body">' +
					'<b class="mui-name" data-userid="' + v.userid + '">' + v.username + '</b>' +
					'【' +
					'<span class="mui-state ">' + v.wstatus + '</span>' +
					'】' +
					' <p class="mui-ellipsis mui-time">' + v.wtime + '</p>' +
					'</div>' +
					'</a>'+
					'</li>';
			container.append($(sLi));
		});
	}
