//初始化
var count = 0;
mui.init({
	swipeBack: false,
	//	pullRefresh: {
	//		container: '#pullrefresh',
	//		down: {
	//			callback: pulldownRefresh
	//		},
	//		up: {
	//			contentrefresh: '正在加载...',
	//			callback: pullupRefresh
	//		}
	//	}
});
/**
 * 下拉刷新具体业务实现
 */
function pulldownRefresh() {
	setTimeout(function() {
		var table = document.body.querySelector('.mui-table-view');
		var cells = document.body.querySelectorAll('.mui-table-view-cell');
		for(var i = cells.length, len = i + 3; i < len; i++) {
			var li = document.createElement('li');
			li.className = 'mui-table-view-cell';
			li.innerHTML = '<a href="javascript:;" class="mui-navigate-right">' +
				'<div class="mui-media-body">' +
				'<b class="mui-name">某某某' + (i + 1) + '</b>' +
				'【' +
				'<span class="mui-state">上班打卡</span>' +
				'】' +
				' <p class="mui-ellipsis mui-time">2018-12-12 12:12:23</p>' +
				'</div>' +
				'</a>';
			//下拉刷新，新纪录插到最前面；
			table.insertBefore(li, table.firstChild);
		}
		mui('#pullrefresh').pullRefresh().endPulldownToRefresh(); //refresh completed
	}, 1000);
}
/**
 * 上拉加载具体业务实现
 */
function pullupRefresh() {
	setTimeout(function() {
		mui('#pullrefresh').pullRefresh().endPullupToRefresh((++count > 2)); //参数为true代表没有更多数据了。
		var table = document.body.querySelector('.mui-table-view');
		var cells = document.body.querySelectorAll('.mui-table-view-cell');
		for(var i = cells.length, len = i + 20; i < len; i++) {
			var li = document.createElement('li');
			li.className = 'mui-table-view-cell';
			li.innerHTML = '<a href="javascript:;" class="mui-navigate-right">' +
				'<div class="mui-media-body">' +
				'<b class="mui-name">某某某' + (i + 1) + '</b>' +
				'【' +
				'<span class="mui-state">上班打卡</span>' +
				'】' +
				' <p class="mui-ellipsis mui-time">2018-12-12 12:12:23</p>' +
				'</div>' +
				'</a>';
			table.appendChild(li);
		}
	}, 1000);
}



/*
 web socket
 * */
WebSocketFn(
	"ws://"+ sIp +":9801/", 
	{
		"type": "getcheckonhistory"
	},
	function(data) {
		var msg = JSON.parse(data)
		createList(msg, document.getElementById("normalUl"));
		createAblist(msg, document.getElementById("abnormalUl"));
		updataList(msg);
	}
);


//创建上下班离开员工列表
function createList(msg, container) {
//	console.log(msg.data.time,msg.data.status);
	if(
		(msg.type == 'person' && msg.status == 'up' && msg.nowstatus == 0) || 
		(msg.type == 'person' && msg.status == 'down' && msg.nowstatus == 0) || 
		(msg.type == 'person' && msg.status == 'now' && msg.nowstatus == 2) || 
		(msg.type == 'person' && msg.status == 'history' && msg.histype == 'up' && msg.data.status == '上班') || 
		(msg.type == 'person' && msg.status == 'history' && msg.histype == 'down' && msg.data.status == '下班') || 
		(msg.type == 'person' && msg.status == 'history' && msg.histype == 'now' && msg.data.status == '离开')
	){
//		console.log(msg.data.time,getOnTime('y-m-d'));
		if(dateTransverter(msg.data.time)){
			if(dateTransverter(msg.data.time) >= dateTransverter(getOnTime('y-m-d'))){
				var sHtml = '<a href="javascript:;" class="mui-navigate-right">' +
					'<div class="mui-media-body">' +
					'<b class="mui-name" data-userid="' + msg.data.userid + '">' + msg.data.username + '</b>' +
					'【' +
					'<span class="mui-state '+ backColclass(msg) +'">' + backState(msg) + '</span>' +
					'】' +
					' <p class="mui-ellipsis mui-time">' + msg.data.time + '</p>' +
					'</div>' +
					'</a>';
		
				var li = document.createElement('li');
				li.className = 'mui-table-view-cell mui-media';
				li.setAttribute('data-devkey', msg.data.devkey);
				li.setAttribute('data-tanba', msg.data.tanba);
				li.innerHTML = sHtml;
				container.insertBefore(li, container.firstChild);
				document.getElementById('normalCount').innerHTML = '('+ mui('#normalUl li').length +')';
			}
		}
		
	}

}

//创建异常状态员工列表
function createAblist(msg, container) {
//	console.log(msg.data.time,msg.data.status)
	if(
	   (msg.type == 'person' && msg.status == 'up' && msg.nowstatus == 1) ||
	   (msg.type == 'person' && msg.status == 'down' && msg.nowstatus == 1) || 
	   (msg.type == 'person' && msg.status == 'now' && msg.nowstatus == 1) || 
	   (msg.type == 'person' && msg.status == 'history' && msg.histype == 'up' && msg.data.status == '迟到') || 
	   (msg.type == 'person' && msg.status == 'history' && msg.histype == 'down' && msg.data.status == '早退') || 
	   (msg.type == 'person' && msg.status == 'history' && msg.histype == 'now' && msg.data.status == '不在') || 
	   (msg.type == 'person' && msg.status == 'history' && msg.histype == 'now' && msg.data.status == '缺勤')
	){
		
			var sHtml = '<a href="javascript:;" class="mui-navigate-right">' +
				'<div class="mui-media-body">' +
				'<b class="mui-name" data-userid="' + msg.data.userid + '">' + msg.data.username + '</b>' +
				'【' +
				'<span class="mui-state '+ backColclass(msg) +'">' + backState(msg) + '</span>' +
				'】' +
				' <p class="mui-ellipsis mui-time">' + msg.data.time + '</p>' +
				'</div>' +
				'</a>';
	
			var li = document.createElement('li');
			li.className = 'mui-table-view-cell mui-media';
			li.setAttribute('data-devkey', msg.data.devkey);
			li.setAttribute('data-tanba', msg.data.tanba);
			li.innerHTML = sHtml;
			container.insertBefore(li, container.firstChild);
			document.getElementById('abnormalCount').innerHTML = '('+ mui('#abnormalUl li').length +')';
			
	}

}

//返回字体颜色类名
function backColclass(msg){
	if(msg.data.time)
	return (dateTransverter(msg.data.time) < dateTransverter(getOnTime('y-m-d'))) ? "c-gray" : "c-black";
}

//员工状态
function backState(msg) {
	
	switch(true) {
		//正常考勤
		case(msg.type == 'person' && msg.status == 'up' && msg.nowstatus == 0):
			return '<span class="s-up-1">上班打卡</span>';
			break;
		case(msg.type == 'person' && msg.status == 'down' && msg.nowstatus == 0):
			return '<span class="s-down-1">下班打卡</span>';
			break;
		case(msg.type == 'person' && msg.status == 'now' && msg.nowstatus == 2):
			return '<span class="s-leave">离开</span>';
			break;
		case(msg.type == 'person' && msg.status == 'history' && msg.histype == 'up' && msg.data.status == '上班'):
			return '<span class="s-up-1">上班打卡</span>';
			break;
		case(msg.type == 'person' && msg.status == 'history' && msg.histype == 'down' && msg.data.status == '下班'):
			return '<span class="s-down-1">下班打卡</span>';
			break;
		case(msg.type == 'person' && msg.status == 'history' && msg.histype == 'now' && msg.data.status == '离开'):
			return '<span class="s-leave">离开</span>';
			break;
		//异常
		case(msg.type == 'person' && msg.status == 'up' && msg.nowstatus == 1):
			return '<span class="s-up-2">迟到</span>';
			break;
		case(msg.type == 'person' && msg.status == 'down' && msg.nowstatus == 1):
			return '<span class="s-down-2">早退</span>';
			break;
		case(msg.type == 'person' && msg.status == 'now' && msg.nowstatus == 1):
			return '<span class="s-out">不在</span>';
			break;
		case(msg.type == 'person' && msg.status == 'history' && msg.histype == 'up' && msg.data.status == '迟到'):
			return '<span class="s-up-2">迟到</span>';
			break;
		case(msg.type == 'person' && msg.status == 'history' && msg.histype == 'down' && msg.data.status == '早退'):
			return '<span class="s-down-2">早退</span>';
			break;
		case(msg.type == 'person' && msg.status == 'history' && msg.histype == 'now' && msg.data.status == '不在'):
			return '<span class="s-out">不在</span>';
			break;
		case(msg.type == 'person' && msg.status == 'history' && msg.histype == 'now' && msg.data.status == '缺勤'):
			return '<span class="s-queqin">缺勤</span>';
			break;
	}
}

//ws请求函数
function WebSocketFn(sUrl, data, fn) {
	if(typeof WebSocket != 'undefined') {
		mui.toast("游览器支持 WebSocket!");

		//实例化断开重连对象
		var ws = new ReconnectingWebSocket(sUrl);
		ws.onopen = function() {
			ws.send(JSON.stringify(data));
			console.log("数据发送中...");
		};

		ws.onmessage = function(evt) {
			var received_toast = evt.data;
			fn(received_toast);
			if(JSON.parse(received_toast).status == "historyend"){
				ws.send(JSON.stringify({"type":"getcheckon"}));
			}
			mui('#normalUl li').length >= 100 && (document.getElementById("normalUl").innerHTML = "");
			mui('#abnormalUl li').length >= 100 && (document.getElementById("abnormalUl").innerHTML = "");
		};

		ws.onclose = function() {
			// 关闭 websocket
			ws.close();
			mui.toast("连接已关闭...");
		};

		//监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
		window.onbeforeunload = function() {
			ws.close();
		}

	} else {
		mui.alert("该浏览器不支持查看实时数据，请更新到最新版本");
	}
}

//更新数据
function updataList(msg){
	if(msg.type == "person" && msg.status == "now"){
		if(msg.nowstatus == 0){//在
			$('#normalUl li,#abnormalUl li').each(function(i){
				if(dateTransverter($(this).find('.mui-time').text()) <= dateTransverter(msg.data.time) && $(this).find('.s-leave').text() == "离开"){
					$(this).remove();
				}
			});
		}
		else if(msg.nowstatus == 1){//不在
			
		}
		else if(msg.nowstatus == 2){//离开
			
		}
	}
}
