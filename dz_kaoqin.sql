/*
Navicat MySQL Data Transfer

Source Server         : 103.251.36.122
Source Server Version : 50720
Source Host           : 103.251.36.122:51618
Source Database       : hrdataserver

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-05-09 14:44:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for a000
-- ----------------------------
DROP TABLE IF EXISTS `a000`;
CREATE TABLE `a000` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `tbid` varchar(50) NOT NULL DEFAULT '0',
  `bssid` varchar(250) DEFAULT NULL,
  `ssid_len` varchar(10) DEFAULT NULL,
  `ssid` varchar(250) DEFAULT NULL,
  `channel` varchar(250) DEFAULT NULL,
  `rssi` int(4) DEFAULT '0',
  `is_hidden` char(4) DEFAULT NULL,
  `frametype` char(4) DEFAULT '' COMMENT 'Ap连接的AP的mac地址',
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`time`),
  KEY `tbid` (`tbid`),
  KEY `bssid` (`bssid`)
) ENGINE=MyISAM AUTO_INCREMENT=1092 DEFAULT CHARSET=utf8 COMMENT='AP数据表'
/*!50100 PARTITION BY RANGE (time)
(PARTITION a000_p0 VALUES LESS THAN (1514736000) ENGINE = MyISAM,
 PARTITION a000_p201801 VALUES LESS THAN (1517414400) ENGINE = MyISAM,
 PARTITION a000_p201802 VALUES LESS THAN (1519833600) ENGINE = MyISAM,
 PARTITION a000_p201803 VALUES LESS THAN (1522512000) ENGINE = MyISAM,
 PARTITION a000_p201804 VALUES LESS THAN (1525104000) ENGINE = MyISAM,
 PARTITION a000_p201805 VALUES LESS THAN (1527782400) ENGINE = MyISAM) */;

-- ----------------------------
-- Table structure for a001
-- ----------------------------
DROP TABLE IF EXISTS `a001`;
CREATE TABLE `a001` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `tbid` varchar(12) NOT NULL DEFAULT '0' COMMENT '探霸ID',
  `mac` varchar(50) NOT NULL COMMENT 'stamac地址',
  `rssi` int(4) DEFAULT '0' COMMENT '信号强度',
  `Has_ap` char(4) DEFAULT '1' COMMENT 'STA是否连接AP。1：连接AP，0：未连接AP。',
  `ApMac` varchar(50) DEFAULT '' COMMENT 'STA连接的AP的mac地址',
  `frametype` char(4) DEFAULT '' COMMENT 'STA帧类型',
  `time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`time`),
  KEY `tbid` (`tbid`),
  KEY `mac` (`mac`)
) ENGINE=MyISAM AUTO_INCREMENT=2823666 DEFAULT CHARSET=utf8 COMMENT='STA数据表'
/*!50100 PARTITION BY RANGE (time)
(PARTITION a001_p0 VALUES LESS THAN (1514736000) ENGINE = MyISAM,
 PARTITION a001_p201801 VALUES LESS THAN (1517414400) ENGINE = MyISAM,
 PARTITION a001_p201802 VALUES LESS THAN (1519833600) ENGINE = MyISAM,
 PARTITION a001_p201803 VALUES LESS THAN (1522512000) ENGINE = MyISAM,
 PARTITION a001_p201804 VALUES LESS THAN (1525104000) ENGINE = MyISAM,
 PARTITION a001_p201805 VALUES LESS THAN (1527782400) ENGINE = MyISAM) */;

-- ----------------------------
-- Table structure for a002
-- ----------------------------
DROP TABLE IF EXISTS `a002`;
CREATE TABLE `a002` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'deviceAutoID',
  `tbid` varchar(12) NOT NULL DEFAULT '0' COMMENT '探霸ID',
  `deviceID` varchar(100) NOT NULL DEFAULT '' COMMENT '标签名',
  `battery` varchar(50) DEFAULT NULL COMMENT '标签电量',
  `sendRssi` int(4) DEFAULT NULL COMMENT '信号强度',
  `rssi` int(4) DEFAULT '0' COMMENT '基础信号参数',
  `urgentStatus` char(2) DEFAULT '0' COMMENT '是否为紧急情况。1：是，0：否。',
  `time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`time`),
  KEY `tbid` (`tbid`),
  KEY `deviceID` (`deviceID`)
) ENGINE=MyISAM AUTO_INCREMENT=2844431 DEFAULT CHARSET=utf8 COMMENT='RF数据表'
/*!50100 PARTITION BY RANGE (time)
(PARTITION a002_p0 VALUES LESS THAN (1514736000) ENGINE = MyISAM,
 PARTITION a002_p201801 VALUES LESS THAN (1517414400) ENGINE = MyISAM,
 PARTITION a002_p201802 VALUES LESS THAN (1519833600) ENGINE = MyISAM,
 PARTITION a002_p201803 VALUES LESS THAN (1522512000) ENGINE = MyISAM,
 PARTITION a002_p201804 VALUES LESS THAN (1525104000) ENGINE = MyISAM,
 PARTITION a002_p201805 VALUES LESS THAN (1527782400) ENGINE = MyISAM) */;

-- ----------------------------
-- Table structure for addin_a000
-- ----------------------------
DROP TABLE IF EXISTS `addin_a000`;
CREATE TABLE `addin_a000` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `tbid` int(11) NOT NULL DEFAULT '0' COMMENT '探霸设备标识',
  `devid` int(11) NOT NULL DEFAULT '0' COMMENT '终端标识-',
  `firsttime` int(11) NOT NULL DEFAULT '0' COMMENT '第一次记录时间',
  `finds` int(11) DEFAULT '1' COMMENT '探测次数',
  `lasttime` int(11) DEFAULT '0' COMMENT '最后记录时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `tbdev` (`tbid`,`devid`,`firsttime`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10364 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='AP新记录';

-- ----------------------------
-- Table structure for addin_a001
-- ----------------------------
DROP TABLE IF EXISTS `addin_a001`;
CREATE TABLE `addin_a001` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `tbid` int(11) NOT NULL DEFAULT '0' COMMENT '探霸设备标识',
  `devid` int(11) NOT NULL DEFAULT '0' COMMENT '终端标识-',
  `firsttime` int(11) NOT NULL DEFAULT '0' COMMENT '第一次记录时间',
  `finds` int(11) DEFAULT '1' COMMENT '探测次数',
  `lasttime` int(11) DEFAULT '0' COMMENT '最后记录时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `tbdev` (`tbid`,`devid`,`firsttime`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=813414 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='STA新记录';

-- ----------------------------
-- Table structure for addin_a002
-- ----------------------------
DROP TABLE IF EXISTS `addin_a002`;
CREATE TABLE `addin_a002` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `tbid` int(11) NOT NULL DEFAULT '0' COMMENT '探霸设备标识',
  `devid` int(11) NOT NULL DEFAULT '0' COMMENT '终端标识-',
  `firsttime` int(11) NOT NULL DEFAULT '0' COMMENT '第一次记录时间',
  `finds` int(11) DEFAULT '1' COMMENT '探测次数',
  `lasttime` int(11) DEFAULT '0' COMMENT '最后记录时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `tbdev` (`tbid`,`devid`,`firsttime`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9678 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='RF新记录';

-- ----------------------------
-- Table structure for adminUser
-- ----------------------------
DROP TABLE IF EXISTS `adminUser`;
CREATE TABLE `adminUser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vUserName` varchar(100) DEFAULT NULL COMMENT '用户名',
  `vPassWord` varchar(100) DEFAULT NULL COMMENT '密码',
  `iRoleId` int(11) DEFAULT NULL COMMENT '外键角色id',
  `dCreatetime` int(11) DEFAULT NULL COMMENT '添加时间',
  `dLastTime` int(11) DEFAULT '0' COMMENT '最后一次登录的时间',
  `vIp` varchar(100) DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`),
  UNIQUE KEY `vUserName` (`vUserName`),
  UNIQUE KEY `vUserName_2` (`vUserName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='后台用户登录表';

-- ----------------------------
-- Table structure for apbase
-- ----------------------------
DROP TABLE IF EXISTS `apbase`;
CREATE TABLE `apbase` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceId` varchar(50) DEFAULT NULL COMMENT '标签',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态0正常1异常',
  `time` int(11) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `did` (`deviceId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8092 DEFAULT CHARSET=utf8 COMMENT='STA基础信息';

-- ----------------------------
-- Table structure for area_info
-- ----------------------------
DROP TABLE IF EXISTS `area_info`;
CREATE TABLE `area_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `dec` varchar(255) NOT NULL COMMENT '//描述，区域信息,建议填地址',
  `province_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属省ID',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '所属城市ID',
  `dis_id` int(11) NOT NULL DEFAULT '0' COMMENT '区/县id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for chartApply
-- ----------------------------
DROP TABLE IF EXISTS `chartApply`;
CREATE TABLE `chartApply` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `vApplyName` varchar(100) DEFAULT NULL COMMENT '应用名字',
  `vImgUrl` varchar(100) DEFAULT NULL COMMENT '图片路径',
  `vPath` varchar(100) DEFAULT NULL COMMENT '连接地址',
  `vDataType` varchar(1) DEFAULT NULL COMMENT '图表类型',
  `iAid` varchar(10) DEFAULT NULL COMMENT '标识id',
  `dTime` datetime DEFAULT NULL COMMENT '创建时间',
  `ifDel` int(1) DEFAULT '1' COMMENT '是否删除 1:没有删除 0:删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COMMENT='图表应用表';

-- ----------------------------
-- Table structure for checklog
-- ----------------------------
DROP TABLE IF EXISTS `checklog`;
CREATE TABLE `checklog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(8) NOT NULL,
  `devmac` varchar(50) NOT NULL,
  `dtype` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型1-a001,2-a002',
  `tbid` int(8) NOT NULL,
  `loginfo` json DEFAULT NULL COMMENT '设备记录tbid-time',
  `lastintime` varchar(50) DEFAULT NULL,
  `check` tinyint(1) DEFAULT '0' COMMENT '0在1不在2离开',
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`time`),
  KEY `tbid` (`tbid`),
  KEY `devmac` (`devmac`),
  KEY `dtype` (`dtype`),
  KEY `check` (`check`)
) ENGINE=InnoDB AUTO_INCREMENT=1262 DEFAULT CHARSET=utf8
/*!50100 PARTITION BY RANGE (time)
(PARTITION a000_p201803 VALUES LESS THAN (1522512000) ENGINE = InnoDB,
 PARTITION a000_p201804 VALUES LESS THAN (1525104000) ENGINE = InnoDB,
 PARTITION a000_p201805 VALUES LESS THAN (1527782400) ENGINE = InnoDB) */;

-- ----------------------------
-- Table structure for checkviplog
-- ----------------------------
DROP TABLE IF EXISTS `checkviplog`;
CREATE TABLE `checkviplog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(8) NOT NULL,
  `devmac` varchar(50) NOT NULL,
  `dtype` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型1-a001,2-a002',
  `tbid` int(8) NOT NULL,
  `loginfo` json DEFAULT NULL COMMENT '设备记录tbid-time',
  `lastintime` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0不在1新进',
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`time`),
  KEY `tbid` (`tbid`),
  KEY `devmac` (`devmac`),
  KEY `dtype` (`dtype`),
  KEY `check` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=20114 DEFAULT CHARSET=utf8
/*!50100 PARTITION BY RANGE (time)
(PARTITION a000_p201803 VALUES LESS THAN (1522512000) ENGINE = InnoDB,
 PARTITION a000_p201804 VALUES LESS THAN (1525104000) ENGINE = InnoDB,
 PARTITION a000_p201805 VALUES LESS THAN (1527782400) ENGINE = InnoDB) */;

-- ----------------------------
-- Table structure for cmdlog
-- ----------------------------
DROP TABLE IF EXISTS `cmdlog`;
CREATE TABLE `cmdlog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `deviceId` varchar(50) NOT NULL COMMENT '设备ID',
  `opSource` tinyint(1) unsigned NOT NULL COMMENT '操作源 1 websocket | 2 System | 3 其他',
  `opUser` tinyint(1) unsigned NOT NULL COMMENT '操作用户',
  `cmdBefore` varchar(100) DEFAULT NULL,
  `cdmAfter` varchar(100) DEFAULT NULL,
  `hashVer` varchar(10) DEFAULT NULL,
  `retryCount` tinyint(1) unsigned DEFAULT NULL COMMENT '重试次数',
  `token` int(11) unsigned DEFAULT NULL COMMENT 'token',
  `createat` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS `device`;
CREATE TABLE `device` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT,
  `mac` varchar(255) DEFAULT '' COMMENT '探霸Mac地址',
  `softver` tinyint(1) DEFAULT NULL COMMENT '软件版本',
  `hardver` tinyint(1) DEFAULT NULL COMMENT '硬件版本',
  `protocolver` tinyint(1) DEFAULT NULL COMMENT '协议版本',
  `datafd` int(11) DEFAULT NULL,
  `cfgfd` int(11) DEFAULT NULL,
  `token` int(11) DEFAULT '0' COMMENT '数据服务器token',
  `cfgtoken` int(11) DEFAULT NULL COMMENT '配置服务器token',
  `serverip` varchar(255) DEFAULT NULL COMMENT '数据服务器IP',
  `cfgserverip` varchar(255) DEFAULT NULL COMMENT '配置服务器IP',
  `serverport` varchar(50) DEFAULT NULL COMMENT '数据服务器端口',
  `cfgserverport` varchar(255) DEFAULT NULL COMMENT '配置服务器端口',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '数据服务器探霸状态数据服务器探霸状态 0正常 1在线 2离线 3故障  4维修中 5报废',
  `cfgstatus` tinyint(1) NOT NULL COMMENT '配置服务器在线状态 1在线 0离线 ',
  `createtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `lasttime` int(11) DEFAULT NULL COMMENT '数据服务器最近一次上线时间',
  `cfglasttime` int(11) DEFAULT NULL COMMENT '配置服务器最后一次上线时间',
  `lastouttime` int(11) DEFAULT NULL COMMENT '数据服务器最近一次下线时间',
  `cfglastouttime` int(11) DEFAULT NULL COMMENT '配置服务器最后一次下线时间',
  `dataheart` int(8) DEFAULT '5000' COMMENT '心跳间隔毫秒',
  `cfgheart` int(8) DEFAULT '5000' COMMENT '配置服务心跳间隔',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mac` (`mac`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=376 DEFAULT CHARSET=utf8 COMMENT='探霸基础信息表';

-- ----------------------------
-- Table structure for device_basehome
-- ----------------------------
DROP TABLE IF EXISTS `device_basehome`;
CREATE TABLE `device_basehome` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `dvid` int(11) NOT NULL DEFAULT '0' COMMENT '探霸id',
  `x` decimal(10,2) DEFAULT '0.00' COMMENT '坐标x',
  `y` decimal(10,2) DEFAULT '0.00' COMMENT '坐标y',
  `z` decimal(10,2) DEFAULT '0.00' COMMENT '坐标z',
  `rn` decimal(10,2) DEFAULT '1.00' COMMENT 'RSSI信号补偿参数倍数',
  `a0` tinyint(3) DEFAULT '45' COMMENT '1m处标准Rssi的A值',
  `ax` tinyint(3) DEFAULT '5' COMMENT 'A值波动范围',
  `areaid` int(8) DEFAULT '0' COMMENT '所属区域',
  `addressinfo` varchar(255) DEFAULT NULL COMMENT '位置说明',
  `longitude` varchar(50) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(50) DEFAULT '' COMMENT '纬度',
  `ismove` tinyint(3) DEFAULT '0' COMMENT '是否是移动探霸，1是0否',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COMMENT='设备定位坐标表';

-- ----------------------------
-- Table structure for device_command_log
-- ----------------------------
DROP TABLE IF EXISTS `device_command_log`;
CREATE TABLE `device_command_log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `tbid` int(11) DEFAULT NULL,
  `command` varchar(255) DEFAULT NULL COMMENT '命令内容',
  `cmdtime` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0' COMMENT '0-成功，1失败',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备命令历史表';

-- ----------------------------
-- Table structure for device_info
-- ----------------------------
DROP TABLE IF EXISTS `device_info`;
CREATE TABLE `device_info` (
  `DeviceID` char(18) NOT NULL DEFAULT '' COMMENT '设备ID',
  `SoftwareVersionMa` varchar(10) NOT NULL DEFAULT '' COMMENT '软件版本号-主',
  `SoftwareVersionMi` varchar(10) NOT NULL DEFAULT '' COMMENT '软件版本号-次',
  `HardwareVersionMa` varchar(10) NOT NULL DEFAULT '' COMMENT '硬件版本号-主',
  `HardwareVersionMi` varchar(10) NOT NULL DEFAULT '' COMMENT '硬件版本号-次',
  `ProtocolVersionMa` varchar(10) NOT NULL DEFAULT '' COMMENT '协议版本号-主',
  `ProtocolVersionMi` varchar(10) NOT NULL DEFAULT '' COMMENT '协议版本号-次',
  `MPid` char(10) NOT NULL DEFAULT '' COMMENT '自增代号',
  PRIMARY KEY (`DeviceID`),
  UNIQUE KEY `code01` (`MPid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备信息表';

-- ----------------------------
-- Table structure for devicelog
-- ----------------------------
DROP TABLE IF EXISTS `devicelog`;
CREATE TABLE `devicelog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志ID',
  `type` varchar(100) DEFAULT NULL COMMENT '日志分类',
  `deviceid` varchar(255) DEFAULT NULL COMMENT '设备ID号',
  `description` varchar(255) DEFAULT NULL COMMENT '日志描述',
  `ip` varchar(15) DEFAULT NULL COMMENT '操作IP',
  `createtime` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5898 DEFAULT CHARSET=utf8 COMMENT='设备链接历史';

-- ----------------------------
-- Table structure for json123
-- ----------------------------
DROP TABLE IF EXISTS `json123`;
CREATE TABLE `json123` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `de` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for node
-- ----------------------------
DROP TABLE IF EXISTS `node`;
CREATE TABLE `node` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `iParentId` int(11) DEFAULT NULL COMMENT '父级id',
  `vName` varchar(100) DEFAULT NULL COMMENT '节点名称',
  `vController` varchar(100) DEFAULT NULL COMMENT '控制器名称',
  `vAction` varchar(100) DEFAULT NULL COMMENT '方法名称',
  `vStr` varchar(100) DEFAULT NULL COMMENT '操作名称',
  `iLevel` varchar(100) DEFAULT NULL COMMENT '节点级别: 1控制器 2方法',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点表';

-- ----------------------------
-- Table structure for oui
-- ----------------------------
DROP TABLE IF EXISTS `oui`;
CREATE TABLE `oui` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `Assignment` varchar(255) NOT NULL,
  `OrganizationName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `AssignmentIKey` (`Assignment`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24233 DEFAULT CHARSET=utf8 COMMENT='mac地址参数表';

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vUserName` varchar(100) NOT NULL COMMENT '姓名',
  `iTel` varchar(11) NOT NULL,
  `iTel1` varchar(11) DEFAULT NULL COMMENT '联系方式',
  `vTher` varchar(20) DEFAULT NULL COMMENT '其他',
  `ileave` tinyint(1) DEFAULT '0' COMMENT '是否离职1离职0在',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COMMENT='人员管理表';

-- ----------------------------
-- Table structure for rate_day
-- ----------------------------
DROP TABLE IF EXISTS `rate_day`;
CREATE TABLE `rate_day` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `locationid` int(10) unsigned NOT NULL COMMENT '位置ID',
  `first` int(11) unsigned NOT NULL COMMENT '第一次出现人数',
  `last` int(11) NOT NULL COMMENT '回头人数',
  `rate` float(11,2) unsigned NOT NULL COMMENT '回头率百分比',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`createtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自然天回头率';

-- ----------------------------
-- Table structure for rate_month
-- ----------------------------
DROP TABLE IF EXISTS `rate_month`;
CREATE TABLE `rate_month` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `locationid` int(11) unsigned DEFAULT NULL COMMENT '位置id',
  `first` int(11) unsigned DEFAULT NULL,
  `last` int(11) unsigned DEFAULT NULL,
  `rate` float(11,10) unsigned NOT NULL COMMENT '回头率百分比',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`createtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自然月回头率';

-- ----------------------------
-- Table structure for rate_quarter
-- ----------------------------
DROP TABLE IF EXISTS `rate_quarter`;
CREATE TABLE `rate_quarter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `locationid` int(11) unsigned DEFAULT NULL COMMENT '位置id',
  `first` int(11) unsigned DEFAULT NULL,
  `last` int(11) unsigned DEFAULT NULL,
  `rate` float(11,10) unsigned NOT NULL COMMENT '回头率百分比',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`createtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自然月回头率';

-- ----------------------------
-- Table structure for rate_week
-- ----------------------------
DROP TABLE IF EXISTS `rate_week`;
CREATE TABLE `rate_week` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `locationid` int(10) unsigned NOT NULL COMMENT '位置ID',
  `first` int(11) unsigned NOT NULL COMMENT '第一次出现人数',
  `last` int(11) NOT NULL COMMENT '回头人数',
  `rate` float(11,2) unsigned NOT NULL COMMENT '回头率百分比',
  `createtime` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`,`createtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自然周回头率';

-- ----------------------------
-- Table structure for rfbase
-- ----------------------------
DROP TABLE IF EXISTS `rfbase`;
CREATE TABLE `rfbase` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `deviceId` varchar(50) DEFAULT NULL COMMENT '标签',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态:0正常|1异常|2报废',
  `time` int(11) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `did` (`deviceId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8459 DEFAULT CHARSET=utf8 COMMENT='STA基础信息';

-- ----------------------------
-- Table structure for rfperson
-- ----------------------------
DROP TABLE IF EXISTS `rfperson`;
CREATE TABLE `rfperson` (
  `Id` int(8) NOT NULL AUTO_INCREMENT,
  `userId` int(8) NOT NULL,
  `mac` varchar(50) NOT NULL DEFAULT '' COMMENT '标签',
  `devvs` varchar(50) DEFAULT NULL COMMENT 'RF版本',
  `time` int(11) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `mac` (`mac`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='STA基础信息';

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vRoleName` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `dCreatetime` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色管理表';

-- ----------------------------
-- Table structure for serverconfig
-- ----------------------------
DROP TABLE IF EXISTS `serverconfig`;
CREATE TABLE `serverconfig` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(50) NOT NULL DEFAULT '' COMMENT '数据服务器ip',
  `dtpost` int(4) DEFAULT '9502' COMMENT '数据服务端口',
  `cfgport` int(4) DEFAULT '0' COMMENT '配置服务端口',
  `redisconfig` varchar(255) DEFAULT NULL COMMENT 'redis服务配置',
  `dbconfig` varchar(255) DEFAULT NULL COMMENT '数据库配置',
  `laststarttime` int(11) DEFAULT '0' COMMENT '最后一次开机时间',
  `lastovertime` varchar(255) DEFAULT NULL COMMENT '最后一次断开时间',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='服务器配置表';

-- ----------------------------
-- Table structure for short_a000
-- ----------------------------
DROP TABLE IF EXISTS `short_a000`;
CREATE TABLE `short_a000` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `tbid` varchar(50) DEFAULT NULL,
  `deviceid` varchar(50) DEFAULT NULL COMMENT '设备ID 可以是RF或者STA设备',
  `findcount` int(11) unsigned DEFAULT NULL COMMENT '发现次数',
  `createtime` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `main` (`tbid`,`deviceid`,`createtime`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AP指定时间探测表';

-- ----------------------------
-- Table structure for short_a000_rssi
-- ----------------------------
DROP TABLE IF EXISTS `short_a000_rssi`;
CREATE TABLE `short_a000_rssi` (
  `id` int(11) unsigned NOT NULL COMMENT '对应主表外键',
  `rssi` text COMMENT 'rssi信号强度集合',
  `kmrssi` text,
  `kmdist` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AP指定时间探测表';

-- ----------------------------
-- Table structure for short_a001
-- ----------------------------
DROP TABLE IF EXISTS `short_a001`;
CREATE TABLE `short_a001` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `tbid` int(8) DEFAULT NULL,
  `deviceid` bigint(20) DEFAULT NULL COMMENT '设备ID 可以是RF或者STA设备',
  `findcount` int(11) unsigned DEFAULT NULL COMMENT '发现次数',
  `createtime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `main` (`tbid`,`deviceid`,`createtime`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=327704 DEFAULT CHARSET=utf8 COMMENT='STA指定时间探测表';

-- ----------------------------
-- Table structure for short_a001_rssi
-- ----------------------------
DROP TABLE IF EXISTS `short_a001_rssi`;
CREATE TABLE `short_a001_rssi` (
  `id` int(11) unsigned NOT NULL COMMENT '对应主表外键',
  `rssi` json DEFAULT NULL COMMENT 'rssi信号强度集合',
  `kmrssi` json DEFAULT NULL,
  `kmdist` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AP指定时间探测表';

-- ----------------------------
-- Table structure for short_a002
-- ----------------------------
DROP TABLE IF EXISTS `short_a002`;
CREATE TABLE `short_a002` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `tbid` varchar(50) DEFAULT NULL,
  `deviceid` varchar(50) DEFAULT NULL COMMENT '设备ID 可以是RF或者STA设备',
  `findcount` int(11) unsigned DEFAULT NULL COMMENT '发现次数',
  `createtime` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `main` (`deviceid`,`createtime`,`tbid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=221768 DEFAULT CHARSET=utf8 COMMENT='RF指定时间探测表';

-- ----------------------------
-- Table structure for short_a002_rssi
-- ----------------------------
DROP TABLE IF EXISTS `short_a002_rssi`;
CREATE TABLE `short_a002_rssi` (
  `id` int(11) unsigned NOT NULL COMMENT '对应主表外键',
  `rssi` text COMMENT 'rssi信号强度集合',
  `kmrssi` text,
  `kmdist` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AP指定时间探测表';

-- ----------------------------
-- Table structure for stabase
-- ----------------------------
DROP TABLE IF EXISTS `stabase`;
CREATE TABLE `stabase` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(50) NOT NULL DEFAULT '' COMMENT '标签',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态0正常1异常',
  `time` int(11) DEFAULT '0' COMMENT '添加时间',
  `ouid` int(8) DEFAULT '0' COMMENT '品牌',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `mac` (`mac`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=906760 DEFAULT CHARSET=utf8 COMMENT='STA基础信息';

-- ----------------------------
-- Table structure for staperson
-- ----------------------------
DROP TABLE IF EXISTS `staperson`;
CREATE TABLE `staperson` (
  `Id` int(8) NOT NULL AUTO_INCREMENT,
  `userId` int(8) NOT NULL,
  `mac` varchar(50) NOT NULL DEFAULT '' COMMENT '标签',
  `tel` varchar(12) NOT NULL,
  `mobtype` varchar(50) DEFAULT '0' COMMENT '手机品牌',
  `mobuiv` varchar(50) DEFAULT NULL COMMENT 'UI版本',
  `mobname` varchar(50) DEFAULT NULL COMMENT '手机型号',
  `mobver` varchar(50) DEFAULT NULL COMMENT '系统版本',
  `time` int(11) DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `mac` (`mac`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='STA基础信息';

-- ----------------------------
-- Table structure for station
-- ----------------------------
DROP TABLE IF EXISTS `station`;
CREATE TABLE `station` (
  `StaMac` varchar(255) NOT NULL COMMENT 'stamac地址',
  `Rssi` varchar(255) DEFAULT NULL COMMENT '信号强度',
  `HasAp` varchar(255) DEFAULT NULL COMMENT 'STA是否连接AP。1：连接AP，0：未连接AP。',
  `ApMac` varchar(255) DEFAULT '' COMMENT 'STA连接的AP的mac地址',
  `AutoID` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `Pid` varchar(255) NOT NULL COMMENT '探霸ID',
  `CreateTime` int(11) unsigned DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`AutoID`),
  KEY `pid02` (`Pid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='STA收集数据表';

-- ----------------------------
-- Table structure for staytime
-- ----------------------------
DROP TABLE IF EXISTS `staytime`;
CREATE TABLE `staytime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `areaid` int(11) NOT NULL,
  `mac` varchar(255) NOT NULL,
  `starttime` int(10) NOT NULL,
  `endtime` int(10) NOT NULL,
  `staytime` int(10) NOT NULL,
  `logdate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sysconfig
-- ----------------------------
DROP TABLE IF EXISTS `sysconfig`;
CREATE TABLE `sysconfig` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `key` (`key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='系统参数配置';

-- ----------------------------
-- Table structure for tbservice
-- ----------------------------
DROP TABLE IF EXISTS `tbservice`;
CREATE TABLE `tbservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `areaid` int(11) NOT NULL COMMENT ' 对应device_basehome表的areaid',
  `staytime` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为未开通服务，1为开通',
  PRIMARY KEY (`id`),
  UNIQUE KEY `areaid` (`areaid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lo` varchar(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `deid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vUserName` varchar(100) DEFAULT NULL COMMENT '用户名',
  `vPwd` varchar(100) DEFAULT NULL COMMENT '密码',
  `vEmail` varchar(20) DEFAULT NULL COMMENT '邮件',
  `dTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='前台用户登录表';

-- ----------------------------
-- Table structure for worklog
-- ----------------------------
DROP TABLE IF EXISTS `worklog`;
CREATE TABLE `worklog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(8) NOT NULL COMMENT '员工编号',
  `tday` varchar(50) NOT NULL DEFAULT '' COMMENT '考勤日期',
  `uptime` varchar(50) DEFAULT NULL COMMENT '上班时间',
  `upstatus` tinyint(1) DEFAULT '0' COMMENT '上班考勤0正常1迟到',
  `uptype` tinyint(1) DEFAULT '2' COMMENT '上班打开类型来源1sta2rf',
  `upinfo` json DEFAULT NULL,
  `downtime` varchar(50) DEFAULT NULL COMMENT ' 下班时间',
  `downstatus` tinyint(1) DEFAULT '0' COMMENT '下班状态1早退0正常下班',
  `downtype` tinyint(1) DEFAULT NULL COMMENT '类型来源1sta2rf',
  `downinfo` json DEFAULT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`,`time`),
  UNIQUE KEY `userId` (`userId`,`tday`) USING BTREE,
  KEY `tday` (`tday`),
  KEY `userId_2` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=638 DEFAULT CHARSET=utf8 COMMENT='考勤记录表';

-- ----------------------------
-- Table structure for workloginfo
-- ----------------------------
DROP TABLE IF EXISTS `workloginfo`;
CREATE TABLE `workloginfo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(8) DEFAULT NULL COMMENT '员工编号',
  `devmac` varchar(100) DEFAULT NULL,
  `tday` varchar(50) DEFAULT '' COMMENT '考勤日期',
  `wtype` tinyint(1) DEFAULT '0' COMMENT '上班0或下班1',
  `status` tinyint(1) DEFAULT '0' COMMENT '0正常1迟到或早退',
  `dtype` tinyint(1) DEFAULT '2' COMMENT '1sta2rf',
  `info` json DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId` (`userId`,`tday`,`devmac`,`time`) USING BTREE,
  KEY `userId_2` (`userId`),
  KEY `devmac` (`devmac`),
  KEY `tday` (`tday`),
  KEY `time` (`time`),
  KEY `dtype` (`dtype`),
  KEY `wtype` (`wtype`)
) ENGINE=InnoDB AUTO_INCREMENT=1514 DEFAULT CHARSET=utf8 COMMENT='考勤打卡详细记录表';

-- ----------------------------
-- View structure for a001vt
-- ----------------------------
DROP VIEW IF EXISTS `a001vt`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `a001vt` AS select `a001`.`id` AS `id`,`a001`.`mac` AS `mac`,`a001`.`rssi` AS `rssi`,`a001`.`Has_ap` AS `Has_ap`,`a001`.`ApMac` AS `ApMac`,`a001`.`tbid` AS `deviceID`,`a001`.`time` AS `time` from `a001` order by `a001`.`time` desc limit 2000 ;

-- ----------------------------
-- View structure for impactView
-- ----------------------------
DROP VIEW IF EXISTS `impactView`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `impactView` AS select `t1`.`mac` AS `tmac`,`t2`.`mac` AS `mac`,`t1`.`tbid` AS `tbid1`,`t1`.`time` AS `time1`,`t2`.`tbid` AS `tbid2`,`t2`.`time` AS `time2` from (((select `dataserver`.`a001`.`mac` AS `mac`,`dataserver`.`a001`.`tbid` AS `tbid`,`dataserver`.`a001`.`time` AS `time` from `dataserver`.`a001` where ((`dataserver`.`a001`.`time` between (1514260520 - 300) and (1514260520 + 300)) and (`dataserver`.`a001`.`tbid` in (341,349,350))))) `t1` join (select `dataserver`.`a001`.`mac` AS `mac`,`dataserver`.`a001`.`tbid` AS `tbid`,`dataserver`.`a001`.`time` AS `time` from `dataserver`.`a001` where ((`dataserver`.`a001`.`time` between (1511330088 - 300) and (1511330088 + 300)) and (`dataserver`.`a001`.`tbid` in (191,189)))) `t2`) where (`t1`.`mac` = `t2`.`mac`) ;

-- ----------------------------
-- Procedure structure for 333333
-- ----------------------------
DROP PROCEDURE IF EXISTS `333333`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `333333`(IN `dddd` int)
BEGIN
	#Routine body goes here...
select dddd;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ff
-- ----------------------------
DROP PROCEDURE IF EXISTS `ff`;
DELIMITER ;;
CREATE DEFINER=`duzun`@`%` PROCEDURE `ff`(IN `jsonstr` longtext,OUT `retr` int)
BEGIN
	#Routine body goes here...
select jsonstr;
set retr=10;
select retr;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for getImpaction
-- ----------------------------
DROP PROCEDURE IF EXISTS `getImpaction`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getImpaction`(queryjson json,timepd INT,impactcount INT)
BEGIN
	DECLARE rt1 INT DEFAULT 0;
  DECLARE js json;
  DECLARE i int DEFAULT 0;
  declare lens int DEFAULT 0;
  DECLARE kyp VARCHAR(10);

-- call getImpaction("[{\"addr\":\"5\",\"time\":1512086400},{\"addr\":\"4\",\"time\":1514525600}]",30,3)
  DECLARE strt text DEFAULT '';
  DECLARE wheresql text DEFAULT '';

  DECLARE addr INT;
  DECLARE time INT;
 
SELECT json_length(queryjson) into lens;
set i=0;
WHILE i<lens do
	
	set kyp=CONCAT("$[",i,"]");
	SELECT JSON_EXTRACT(queryjson ,kyp) into js; 

    SELECT JSON_EXTRACT(js,'$.addr') into addr;
    SELECT JSON_EXTRACT(js,'$.time') into time;

 -- SELECT mac, COUNT(*) as tcount, time FROM `a001` WHERE  time BETWEEN (time - timepd) AND (time+timepd) AND tbid IN (341, 349, 350)  GROUP BY mac   HAVING tcount >= impactcount;

	set wheresql= CONCAT(wheresql," (time BETWEEN (",time," -", timepd,") AND ( ",time," + ",timepd,") AND tbid IN (341, 349, 350) ) OR");	         
-- addr

	set i = i +1;
end while;			

-- addr
	set strt= CONCAT('SELECT mac, COUNT(*) as tcount,tbid, time FROM `a001` WHERE  ',wheresql,' 1=1 GROUP BY mac   HAVING tcount >= ',impactcount);
 -- SELECT strt;
 

SET sql_mode='';
   SET @lastdata = strt;
   PREPARE lastdata FROM @lastdata;
   EXECUTE lastdata;

  DEALLOCATE PREPARE lastdata;

  END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for getImpaction_old
-- ----------------------------
DROP PROCEDURE IF EXISTS `getImpaction_old`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `getImpaction_old`(queryjson json,timepd INT,impactcount INT)
BEGIN

    DECLARE sTemp1 JSON;
    DECLARE sTemp2 JSON;

    DECLARE addr1 INT;
    DECLARE time1 INT;
    DECLARE addr2 INT;
    DECLARE time2 INT;
		DECLARE stime1 VARCHAR(200);
    DECLARE stime2 VARCHAR(200);

    SELECT JSON_EXTRACT(queryjson,CONCAT("$[0]")) into sTemp1;
    SELECT JSON_EXTRACT(sTemp1,'$.addr') into addr1;
    SELECT JSON_EXTRACT(sTemp1,'$.time') into time1;

-- SELECT data_type(stime1);

    SELECT JSON_EXTRACT(queryjson,CONCAT("$[1]")) into sTemp2;
    SELECT JSON_EXTRACT(sTemp2,'$.addr') into addr2;
    SELECT JSON_EXTRACT(sTemp2,'$.time') into time2;
	
-- SELECT addr1,addr2,time1,time2;

   DROP TEMPORARY TABLE IF EXISTS temp_a001_impact;
		
CREATE TEMPORARY TABLE temp_a001_impact
      SELECT
      t1.mac as tmac ,
      t2.mac,
      t1.tbid as tbid1,
      t1.time as timer1,
      t2.tbid as tbid2,
      t2.time as timer2
    FROM
      (
        SELECT
          mac,
          tbid,
          time
        FROM
          `a001`
        WHERE
          (time BETWEEN (time1 - timepd) AND (time1 + timepd))
AND tbid IN (341, 349, 350)         
-- AND tbid IN (SELECT id from device_basehome WHERE areaid=addr1)
      )
        AS t1
      JOIN
      (
        SELECT
          mac,
          tbid,
          time
        FROM
          `a001`
        WHERE
          (time BETWEEN (time2 - timepd) AND (time2 + timepd)) AND tbid IN (189, 191, 190)
       --   AND tbid IN (SELECT id from device_basehome WHERE areaid=addr2)
      )
        AS t2
    WHERE t1.mac = t2.mac
    ;

-- SELECT * from temp_a001_impact;
  SELECT
      tmac,COUNT(*) as tcount
    FROM temp_a001_impact
    GROUP BY tmac
    HAVING tcount >= impactcount;



    -- 清空系统对账临时表
    truncate table temp_a001_impact;

  END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for jsontest
-- ----------------------------
DROP PROCEDURE IF EXISTS `jsontest`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `jsontest`(IN `jsonstr` longtext,OUT retr INT)
BEGIN
	#Routine body goes here...
DECLARE rt1 INT DEFAULT 0; 
DECLARE js json;
DECLARE js1 json;
DECLARE js2 json;
DECLARE js_key json;
DECLARE i int DEFAULT 0;
DECLARE j int DEFAULT 0;
declare lens int DEFAULT 0;
declare lens1 int DEFAULT 0;
declare lens2 int DEFAULT 0;

DECLARE kyp VARCHAR(10);
DECLARE kyp1 VARCHAR(10);
DECLARE datkey VARCHAR(20);	-- 上报类型 a00  a001 a002
DECLARE tbid VARCHAR(250); -- 探霸id

-- DECLARE errocode INT DEFAULT 0; 
 DECLARE strt LONGTEXT DEFAULT ''; 
 DECLARE strt1 LONGTEXT DEFAULT ''; 
 DECLARE strt2 LONGTEXT DEFAULT ''; 

 DECLARE tmpstr1 VARCHAR(100) DEFAULT ''; 
 DECLARE tmpstr2 VARCHAR(100) DEFAULT ''; 
 DECLARE tmpstr3 VARCHAR(100) DEFAULT ''; 

DROP TEMPORARY TABLE IF EXISTS temp_json;
CREATE TEMPORARY TABLE temp_json (
 `id` INT(11) AUTO_INCREMENT PRIMARY KEY, `myjson` JSON
);
INSERT INTO temp_json(myjson) VALUES (jsonstr);

SELECT json_length(myjson) into lens FROM temp_json;
set i=0;
WHILE i<lens do
	-- 获取第一层包含多少个元素
	set kyp=CONCAT("$[",i,"]");

	SELECT JSON_EXTRACT(myjson,kyp) into js  FROM temp_json; 
	SELECT JSON_EXTRACT(js,'$.tbid') into tbid;-- 取出值

	SELECT JSON_keys(JSON_EXTRACT(js,'$[0][0]')) into js_key;
	SELECT JSON_EXTRACT(js_key,'$[0]') into datkey; -- 取出关键字值

	SELECT JSON_EXTRACT(js,'$[0].*') into js1;	
 SELECT JSON_LENGTH(js1,'$[0]') into lens1;  
	-- select tbid,datkey,js1,lens1;
	set j=0;	
	repeat
		-- 获取第2层包含多少个元素
			SELECT CONCAT("$[0][",j,"][0]") into kyp1;	
			SELECT JSON_EXTRACT(js1,kyp1) into js2;
	 
-- 生成3条insert语句的value
			CASE datkey 
				WHEN '"a000"' THEN 
					
							SELECT JSON_EXTRACT(js2,'$.ssid') into tmpstr2;
							SELECT JSON_EXTRACT(js2,'$.bssid') into tmpstr1;
							set strt = CONCAT(strt,"(",tbid,",",datkey,",",tmpstr1,",",tmpstr2,"),");
				
				WHEN '"a001"' THEN 
				
						SELECT JSON_EXTRACT(js2,'$.mac') into tmpstr1;
						SELECT JSON_EXTRACT(js2,'$.ApMac') into tmpstr2;
						set strt1 = CONCAT(strt1,"(",tbid,",",datkey,",",tmpstr1,",",tmpstr2,"),");
				
				WHEN '"a002"' THEN 
				
						SELECT JSON_EXTRACT(js2,'$.deviceID') into tmpstr1;
						SELECT JSON_EXTRACT(js2,'$.rssi') into tmpstr2;
						set strt2 = CONCAT(strt2,"(",tbid,",",datkey,",",tmpstr1,",",tmpstr2,"),");			
				
				ELSE select 9; 
			END CASE; 

			set j = j+1;
			until j>=lens1
	end repeat;
	set i = i +1;
end while;

set strt= CONCAT('insert into sta (tbid,type,mac,channel) values ',strt);
set strt1= CONCAT('insert into sta (tbid,type,mac,channel) values ',strt1);
set strt2= CONCAT('insert into sta (tbid,type,mac,channel) values ',strt2);

 select left(strt, (length(strt)-1)) into strt;
 select left(strt1, (length(strt1)-1)) into strt1;
 select left(strt2,(length(strt2)-1)) into strt2;
   
 -- 预处理，使执行sql语句字符串
select strt,strt1,strt2;      

SET @lastdata = strt;
PREPARE lastdata FROM @lastdata;   
EXECUTE lastdata;

SET @lastdata = strt1;
PREPARE lastdata FROM @lastdata;   
EXECUTE lastdata;

SET @lastdata = strt2;
PREPARE lastdata FROM @lastdata;   
EXECUTE lastdata;

DEALLOCATE PREPARE lastdata;  

 

set rt1 =last_insert_id(); 
set retr =rt1;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for proeroacount
-- ----------------------------
DROP PROCEDURE IF EXISTS `proeroacount`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `proeroacount`(IN `jsonstr` longtext,OUT retr INT)
BEGIN
	#Routine body goes here...
DECLARE rt1 INT DEFAULT 0; 
DECLARE js json;
DECLARE js1 json;
DECLARE js2 json;
DECLARE js_key json;
DECLARE i int DEFAULT 0;
DECLARE j int DEFAULT 0;
declare lens int DEFAULT 0;
declare lens1 int DEFAULT 0;
declare lens2 int DEFAULT 0;

DECLARE kyp VARCHAR(100);
DECLARE kyp1 VARCHAR(100);
DECLARE datkey VARCHAR(250);	-- 上报类型 a00  a001 a002
DECLARE tbid VARCHAR(250); -- 探霸id

-- DECLARE errocode INT DEFAULT 0; 
 DECLARE strt LONGTEXT DEFAULT ''; 
 DECLARE strt1 LONGTEXT DEFAULT ''; 
 DECLARE strt2 LONGTEXT DEFAULT ''; 

 DECLARE tmpstr1 VARCHAR(100) DEFAULT ''; 
 DECLARE tmpstr2 VARCHAR(100) DEFAULT ''; 
 DECLARE tmpstr3 VARCHAR(100) DEFAULT ''; 

/* [{"TBid":"000007e161020002682e8ae60182137d74d9","cltacount":8},{"TBid":"000007e161020002360b424950ea07e28788","cltacount":6},{"TBid":"000007e1610200021e78b8293f36e0a966e1","cltacount":5}] */
DROP TEMPORARY TABLE IF EXISTS temp_json;
CREATE TEMPORARY TABLE temp_json (
 `id` INT(11) AUTO_INCREMENT PRIMARY KEY, `myjson` JSON
);
INSERT INTO temp_json(myjson) VALUES (jsonstr);

SELECT json_length(myjson) into lens FROM temp_json;

-- PREPARE prod FROM "UPDATE tbinfo SET cltacount=?,cltacountmax=if(cltacount>cltacountmax,cltacount,cltacountmax)  WHERE tbid=?";  
 
set i=0;
WHILE i<lens do
	-- 获取第一层包含多少个元素
	set kyp=CONCAT("$[",i,"]");

	SELECT JSON_EXTRACT(myjson,kyp) into js  FROM temp_json; 
	SELECT JSON_EXTRACT(js,'$.TBid') into tbid;-- 取出值
	SELECT JSON_EXTRACT(js,'$.cltacount') into datkey;-- 取出值


	
   set strt= CONCAT('UPDATE tbinfo SET cltacount=',datkey,',cltacountmax=if(cltacount>cltacountmax,',datkey,',cltacountmax)  WHERE tbid=',tbid);
	 SET @lastdata = strt;
		 PREPARE prod FROM @lastdata;   
		 EXECUTE prod;
/**/
select tbid,datkey,lens,@q2;
		/*
		SET @q1=datkey; 
		SET @q2=tbid;  
 
		EXECUTE prod USING @q1,@q2;
*/
	set i = i +1;
end while;
 DEALLOCATE PREPARE prod ;  

		

set rt1 =last_insert_id(); 
select rt1;
set retr =rt1;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for setpartitionplan
-- ----------------------------
DROP PROCEDURE IF EXISTS `setpartitionplan`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `setpartitionplan`(IN `dtable` VARCHAR(50))
BEGIN	
set @p_name =  CONCAT(dtable,'_p',date_format(DATE_ADD(curdate(),interval 2 MONTH),'%Y%m'));
SET @exist=(
SELECT  
  COUNT(*)  
FROM  
  INFORMATION_SCHEMA.partitions   
WHERE  
  TABLE_SCHEMA = schema()   
  AND TABLE_NAME= dtable AND partition_name=@p_name);
SELECT @exist,@p_name;
 if @exist <1 then 	
-- 提前建立包含当前时间的分区
	set @p_date =  date_format(DATE_ADD(curdate(),interval 3 MONTH),'%Y-%m-01');
	set @p_sql = CONCAT('ALTER TABLE ',dtable,' ADD PARTITION(PARTITION ',@p_name,' VALUES LESS THAN ( unix_timestamp(\'',@p_date,'\')))'); 
	PREPARE stmt FROM @p_sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
end if;

-- 清除半年以前的数据和分区
	set @oldp_name =  CONCAT(dtable,'_p',date_format(DATE_ADD(curdate(),interval -7 MONTH),'%Y%m'));
	SET @exist=(
	SELECT  
		COUNT(*)  
	FROM  
		INFORMATION_SCHEMA.partitions   
	WHERE  
		TABLE_SCHEMA = schema()   
		AND TABLE_NAME= dtable AND partition_name=@oldp_name);
	SELECT @exist,@oldp_name;
 if @exist >0 then 
	 set @p_sql = CONCAT('ALTER TABLE ',dtable,' DROP PARTITION ',@oldp_name); 
	 PREPARE stmt FROM @p_sql;
	 EXECUTE stmt;
	 DEALLOCATE PREPARE stmt;
	end if;

-- call setpartitionplan('a000')
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for stayTimeStatistics
-- ----------------------------
DROP PROCEDURE IF EXISTS `stayTimeStatistics`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `stayTimeStatistics`()
BEGIN
	#Routine body goes here...
	SET @startTime = UNIX_TIMESTAMP(TIMESTAMP(ADDDATE(DATE(SYSDATE()),-1)));
	SET @endTime  = UNIX_TIMESTAMP(DATE(SYSDATE()));
	
	SELECT areaid FROM tbservice WHERE staytime_sta=1;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for swooletanba
-- ----------------------------
DROP PROCEDURE IF EXISTS `swooletanba`;
DELIMITER ;;
CREATE DEFINER=`duzun`@`%` PROCEDURE `swooletanba`(IN `jsonstr` longtext,OUT `retr` int)
BEGIN
DECLARE rt1 INT DEFAULT 0; 
DECLARE js json;
DECLARE js1 json;
DECLARE js2 json;
DECLARE js_key json;
DECLARE i int DEFAULT 0;
DECLARE j int DEFAULT 0;
declare lens int DEFAULT 0;
declare lens1 int DEFAULT 0;
declare lens2 int DEFAULT 0;

DECLARE kyp VARCHAR(10);
DECLARE kyp1 VARCHAR(10);
DECLARE datkey VARCHAR(20);	-- 上报类型 a00  a001 a002
DECLARE tbid VARCHAR(250); -- 探霸id
DECLARE timer VARCHAR(250); -- 探霸id

-- DECLARE errocode INT DEFAULT 0; 
 DECLARE strt LONGTEXT DEFAULT ''; 
 DECLARE strt1 LONGTEXT DEFAULT ''; 
 DECLARE strt2 LONGTEXT DEFAULT ''; 

 DECLARE tmpstr1 VARCHAR(100) DEFAULT ''; 
 DECLARE tmpstr2 VARCHAR(100) DEFAULT ''; 
 DECLARE tmpstr3 VARCHAR(100) DEFAULT ''; 

DROP TEMPORARY TABLE IF EXISTS temp_json;
CREATE TEMPORARY TABLE temp_json (
 `id` INT(11) AUTO_INCREMENT PRIMARY KEY, `myjson` JSON
);
INSERT INTO temp_json(myjson) VALUES (jsonstr);

SELECT json_length(myjson) into lens FROM temp_json;
set i=0;
WHILE i<lens do
	-- 获取第一层包含多少个元素
	set kyp=CONCAT("$[",i,"]");

	SELECT JSON_EXTRACT(myjson,kyp) into js  FROM temp_json; 
	SELECT JSON_EXTRACT(js,'$.tbid') into tbid;-- 取出值
	SELECT JSON_EXTRACT(js,'$.time') into timer;-- 取出值


	SELECT JSON_keys(JSON_EXTRACT(js,'$[0][0]')) into js_key;
	SELECT JSON_EXTRACT(js_key,'$[0]') into datkey; -- 取出关键字值

	SELECT JSON_EXTRACT(js,'$[0].*') into js1;	
 SELECT JSON_LENGTH(js1,'$[0]') into lens1;  

	set j=0;	
	repeat
		-- 获取第2层包含多少个元素
			SELECT CONCAT("$[0][",j,"][0]") into kyp1;	
			SELECT JSON_EXTRACT(js1,kyp1) into js2;
	 
-- 生成3条insert语句的value
			CASE datkey 
				WHEN '"a000"' THEN 
							set strt = CONCAT(strt,"(",tbid,",",datkey,",",timer,",",JSON_EXTRACT(js2,'$.ssid'),",",JSON_EXTRACT(js2,'$.bssid'),"),");
				
				WHEN '"a001"' THEN 
						set strt1 = CONCAT(strt1,"(",tbid,",",datkey,",",timer,",",JSON_EXTRACT(js2,'$.mac'),",", JSON_EXTRACT(js2,'$.ApMac'),"),");
				
				WHEN '"a002"' THEN 
						set strt2 = CONCAT(strt2,"(",tbid,",",datkey,",",timer,",",JSON_EXTRACT(js2,'$.deviceID'),",",JSON_EXTRACT(js2,'$.rssi'),"),");			
				
				ELSE select 9; 
			END CASE; 
			
			set j = j+1;
			until j>=lens1
	end repeat;
	set i = i +1;
end while;

 if strt<>'' then 
	set strt= CONCAT('insert into sta (tbid,type,CreateTime,mac,channel) values ',strt);
	select left(strt, (length(strt)-1)) into strt;
	SET @lastdata = strt;
	PREPARE lastdata FROM @lastdata;   
	EXECUTE lastdata;
end if;
 
 if strt1<>'' then 
	set strt1= CONCAT('insert into sta (tbid,type,CreateTime,mac,channel) values ',strt1);

	select left(strt1,(length(strt1)-1)) into strt1;
	SET @lastdata = strt1;
	PREPARE lastdata FROM @lastdata;   
	EXECUTE lastdata;
end if;

 if strt2<>'' then 
	set strt2= CONCAT('insert into sta (tbid,type,CreateTime,mac,channel) values ',strt2);
	select left(strt2,(length(strt2)-1)) into strt2;

	SET @lastdata = strt2;
	PREPARE lastdata FROM @lastdata;   
	EXECUTE lastdata;
end if;

DEALLOCATE PREPARE lastdata;  
 

set rt1 =last_insert_id(); 
   
SELECT rt1;

set retr =rt1;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for tanbabase
-- ----------------------------
DROP PROCEDURE IF EXISTS `tanbabase`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `tanbabase`(IN `dtype` VARCHAR(50),IN `jsonstr` longtext)
BEGIN
  DECLARE rt1 INT DEFAULT 0;
  DECLARE js json;
  DECLARE i int DEFAULT 0;
  declare lens int DEFAULT 0;

  DECLARE kyp VARCHAR(10);

  DECLARE tbid INT(8); -- 探霸id
  DECLARE deviceid BIGINT(50); -- 探霸id
  DECLARE fdcount INT(11); -- 探霸id
  DECLARE ftime INT(11); -- 探霸id
  DECLARE rssi text;
  DECLARE kmrssi text;
  DECLARE kmdis text ;

  DECLARE strt text DEFAULT '';
  DECLARE strt2 text DEFAULT '';
  -- DECLARE errocode INT DEFAULT 0;

  DECLARE insertid int DEFAULT 0;
  DECLARE tabalname VARCHAR(50);
  DECLARE tabalname1 VARCHAR(50);


  set tabalname= CONCAT("short_",dtype);
  set tabalname1= CONCAT(tabalname,'_rssi');


  SELECT json_length(jsonstr) into lens;
  set i=0;
  WHILE i<lens do
   -- 获取第一层包含多少个元素
   set kyp=CONCAT("$[",i,"]");
   SELECT JSON_EXTRACT(jsonstr,kyp) into js;

   SELECT JSON_EXTRACT(js,'$.tbid') into tbid;
   SELECT JSON_EXTRACT(js,'$.deviceid') into deviceid;
   SELECT JSON_EXTRACT(js,'$.findcount') into fdcount;

   SELECT JSON_EXTRACT(js,'$.createtime') into ftime;
   SELECT JSON_EXTRACT(js,'$.rssi') into rssi;

   SELECT JSON_EXTRACT(js,'$.kmrssi') into kmrssi;
   SELECT JSON_EXTRACT(js,'$.kmdist') into kmdis;

   -- select deviceid,tbid,ftime,fdcount,rssi;
   set strt= CONCAT('insert into ',tabalname,' (tbid,deviceid,findcount,createtime) values (',tbid,',',deviceid,',',fdcount,',',ftime,')');
   SET @lastdata = strt;
   PREPARE lastdata FROM @lastdata;
   EXECUTE lastdata;

   set insertid =last_insert_id();

   set strt2= CONCAT('insert into ',tabalname1," (id,rssi,kmrssi,kmdist) values (",insertid,",",rssi,",",kmrssi,",",kmdis,")");
   SET @lastdata = strt2;
   PREPARE lastdata FROM @lastdata;
   EXECUTE lastdata;

   set i = i +1;
  end while;

  DEALLOCATE PREPARE lastdata;

 END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for findAreaNodes
-- ----------------------------
DROP FUNCTION IF EXISTS `findAreaNodes`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `findAreaNodes`(rootId INT) RETURNS varchar(1000) CHARSET utf8
BEGIN
  DECLARE sTemp VARCHAR(1000);
  DECLARE sTempChd VARCHAR(1000);

  SET sTemp = '';
  SET sTempChd =cast(rootId as CHAR);

  WHILE sTempChd is not null DO
   SET sTemp = concat(sTemp,',',sTempChd);
   SELECT group_concat(id) INTO sTempChd FROM area_info where FIND_IN_SET(parent_id,sTempChd)>0;
  END WHILE;
  RETURN sTemp;
 END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for findAreaParents
-- ----------------------------
DROP FUNCTION IF EXISTS `findAreaParents`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `findAreaParents`(rootId INT) RETURNS varchar(1000) CHARSET utf8
BEGIN
	DECLARE sTemp VARCHAR(1000);
  DECLARE sTempChd VARCHAR(1000);

  SET sTemp = '';
  SET sTempChd =cast(rootId as CHAR);

  WHILE sTempChd is not null DO
   SET sTemp = concat(sTemp,',',sTempChd);
	 SELECT group_concat(parent_id) INTO sTempChd FROM area_info WHERE FIND_IN_SET(id,sTempChd) AND parent_id > 0;
  END WHILE;
  RETURN sTemp;
END
;;
DELIMITER ;

-- ----------------------------
-- Event structure for a000分区管理
-- ----------------------------
DROP EVENT IF EXISTS `a000分区管理`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` EVENT `a000分区管理` ON SCHEDULE EVERY 1 MONTH STARTS '2018-04-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO -- 建立2个月后的分区，删除6个月以前的分区
CALL setpartitionplan('a000')
;;
DELIMITER ;

-- ----------------------------
-- Event structure for a001分区管理
-- ----------------------------
DROP EVENT IF EXISTS `a001分区管理`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` EVENT `a001分区管理` ON SCHEDULE EVERY 1 MONTH STARTS '2018-04-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO CALL setpartitionplan('a001')
;;
DELIMITER ;

-- ----------------------------
-- Event structure for a002分区管理
-- ----------------------------
DROP EVENT IF EXISTS `a002分区管理`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` EVENT `a002分区管理` ON SCHEDULE EVERY 1 MONTH STARTS '2018-04-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO CALL setpartitionplan('a002')
;;
DELIMITER ;
